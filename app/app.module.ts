import { NgModule }       from '@angular/core';
import { RouterModule }   from '@angular/router';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { AppComponent }        from './app.component';
import { HeroDetailComponent } from './heroes/hero-detail.component';
import { HeroesComponent }     from './heroes/heroes.component';
import { HeroService }         from './heroes/hero.service';
import { DashboardComponent } from './dashboard/dashboard.component'



@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
      {
  path: 'detail/:id',
  component: HeroDetailComponent
},
      {
  path: '',
  redirectTo: '/dashboard',
  pathMatch: 'full'
},
      {
  path: 'dashboard',
  component: DashboardComponent
},
  {
    path: 'heroes',
    component: HeroesComponent
  }
])
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    HeroDetailComponent,
    HeroesComponent
  ],
  providers: [
    HeroService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
}
